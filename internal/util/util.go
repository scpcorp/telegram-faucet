package util

import (
	"fmt"
	"strconv"

	"github.com/dustin/go-humanize"
)

// F formats floats with commas, truncating as requested
func F(f float64, truncate bool) string {
	if truncate && f >= 0.01 {
		return humanize.CommafWithDigits(f, 2)
	}
	return humanize.Commaf(f)
}

// I formats ints with commas
func I(i int) string {
	return humanize.Comma(int64(i))
}

var modifiers = []string{"", "k", "M", "G", "T", "P", "E", "Z"}

// PrintHash prints a hash formatted as a string with common SI units
func PrintHash(hashRaw string) string {
	hash, _ := strconv.ParseFloat(hashRaw, 64)
	factor := 1.0
	var modifier string
	for _, modifier = range modifiers {
		nextFactor := factor * 1000
		if hash < nextFactor {
			break
		}
		factor = nextFactor
	}
	return fmt.Sprintf("%.2f %s%s", hash/factor, modifier, "H")
}
