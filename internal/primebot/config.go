package primebot

import (
	"encoding/json"
	"io/ioutil"

	"github.com/sirupsen/logrus"
)

// Read the provided configuration file
func loadConfig(filePath string) Config {
	logrus.Info("Loading Primebot configuration.")
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		logrus.Fatalf("Error: %v", err)
	}
	config := Config{}
	err = json.Unmarshal(data, &config)
	if err != nil {
		logrus.Fatalf("Error: %v", err)
	}
	return config
}
