package primebot

import (
	"gitlab.com/SiaPrime/telegram-faucet/pkg/coingecko"
)

// Config is necessary configuration for the service, loaded from a json file
type Config struct {
	Disabled               bool           `json:"disabled"`
	TelegramToken          string         `json:"telegram_token"`
	UserReferralLink       string         `json:"user_referral_link"`
	UsersPath              string         `json:"users_path"`
	UsagePath              string         `json:"usage_path"`
	Sia                    SiaConfig      `json:"sia"`
	Rewards                RewardsConfig  `json:"rewards"`
	Coingecko              coingecko.Coin `json:"coingecko"`
	TrackedGroupInviteLink string         `json:"tracked_group_invite_link"`
	TrackedGroupID         string         `json:"tracked_group_id"`
	AdminGroupID           string         `json:"admin_group_id"`
	AdminIDs               []string       `json:"admin_ids"`
}

// SiaConfig configures access with the daemon's api
type SiaConfig struct {
	Address   string `json:"address"`
	Password  string `json:"password"`
	UserAgent string `json:"user_agent"`
}

// RewardsConfig sets the payout amounts for reward events
type RewardsConfig struct {
	Join     uint64 `json:"join"`
	Referral uint64 `json:"referral"`
}
