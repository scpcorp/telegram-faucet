package handlers

import (
	"fmt"

	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
)

// Register associates a wallet with a user
func (hm *HandlerManager) Register(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Register++
	hm.Users.LoadOrCreate(m.Sender.ID)
	valid, err := hm.Sia.IsValidAddr(m.Payload)
	if err != nil {
		logrus.Error(err)
		hm.Bot.Send(m.Sender, "I've had trouble verifying that address. Please try again later.")
		return
	}
	if !valid {
		hm.Bot.Send(m.Sender, "It looks like the wallet you provided is invalid.")
		hm.Bot.Send(m.Sender, "You can learn how to create a wallet address here: https://medium.com/@gadaboy11/getting-started-with-the-scprime-ui-b2e26ebd02f5 ")
		return
	}
	// TODO: Send this to the approvers room
	if hm.Users.IsWalletInUse(m.Payload) {
		msg := fmt.Sprintf("A user (name: %s, id: %d) tried to register a wallet that's already in use (wallet: %s) (userobj:%+v).", m.Sender.Username, m.Sender.ID, m.Payload, m.Sender)
		logrus.Error(msg)
		hm.Bot.Send(hm.AdminGroup, msg)
	}
	hm.Users.SetWallet(m.Sender.ID, m.Payload)
	hm.Bot.Send(m.Sender, "I've registered that wallet with your telegram account.")
	hm.StartCheckStatus(m)
}
