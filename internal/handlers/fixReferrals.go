package handlers

import (
	"fmt"

	"gitlab.com/SiaPrime/telegram-faucet/internal/users"
	tb "gopkg.in/tucnak/telebot.v2"
)

// FixReferrals makes sure referrers have been rewarded
func (hm *HandlerManager) FixReferrals(m *tb.Message) {
	hm.Usage.Report.CommandUsage.FixReferrals++
	usersWithoutReferralReward := hm.Users.IdentifyUsersMissingReferral()
	msg := fmt.Sprintf("Identified %d referred users that did not have a reward for their referrer.",
		len(usersWithoutReferralReward))
	hm.Bot.Send(hm.AdminGroup, msg)
	rewardedReferrers := 0
	for _, user := range usersWithoutReferralReward {
		if ok := hm.rewardReferrerLocked(user); ok {
			rewardedReferrers++
		}
	}
	msg = fmt.Sprintf("Rewarded the referrers for %d of those users.", rewardedReferrers)
	hm.Bot.Send(hm.AdminGroup, msg)
}

func (hm *HandlerManager) rewardReferrerLocked(user *users.User) bool {
	hm.RewardMtx.Lock()
	defer hm.RewardMtx.Unlock()
	return hm.rewardReferrer(user, false)
}
