package handlers

import (
	"fmt"

	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
)

// Balance checks the balance of the associated wallet
func (hm *HandlerManager) Balance(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Balance++
	balHastings, err := hm.Sia.Balance()
	balCoins := hm.Sia.HastingsToCoins(balHastings)
	if err != nil {
		logrus.Error(err)
		return
	}
	hm.Bot.Send(m.Sender, fmt.Sprintf("Your balance is %s (%s hastings)", balCoins, balHastings))
}
