package handlers

import (
	"fmt"

	tb "gopkg.in/tucnak/telebot.v2"
)

// ID sends the ID of the sender and chat it is invoked in
func (hm *HandlerManager) ID(m *tb.Message) {
	hm.Usage.Report.CommandUsage.ID++
	hm.Bot.Send(m.Sender,
		fmt.Sprintf("Current chat ID: %v\nSender ID: %v", m.Chat.ID, m.Sender.ID))
}
