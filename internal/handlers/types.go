package handlers

import (
	"sync"

	"gitlab.com/SiaPrime/telegram-faucet/internal/usage"
	"gitlab.com/SiaPrime/telegram-faucet/internal/users"
	"gitlab.com/SiaPrime/telegram-faucet/pkg/coingecko"
	"gitlab.com/SiaPrime/telegram-faucet/pkg/sia"
	tb "gopkg.in/tucnak/telebot.v2"
)

// HandlerManager manages the various command handlers
type HandlerManager struct {
	Disabled               bool
	RewardMtx              sync.Mutex
	Bot                    *tb.Bot
	CG                     *coingecko.Client
	Sia                    *sia.Client
	UserReferralLink       string
	Users                  *users.Users
	TrackedGroup           *tb.Chat
	AdminGroup             *tb.Chat
	TrackedGroupInviteLink string
	JoinReward             uint64
	ReferralReward         uint64
	Usage                  *usage.Usage
}
