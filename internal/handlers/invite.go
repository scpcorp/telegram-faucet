package handlers

import (
	"fmt"

	tb "gopkg.in/tucnak/telebot.v2"
)

// Invite allows a user to get their invite link to share.
func (hm *HandlerManager) Invite(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Invite++
	u, _ := hm.Users.LoadOrCreate(m.Sender.ID)
	// We intentionally still provide the referral link, as it doesn't hurt to get more people,
	// we just need to make it clear that you only get rewards if you qualify too.
	notInGroup := !hm.inGroup(u)
	if notInGroup {
		hm.Bot.Send(m.Sender,
			fmt.Sprintf("You must be in our telegram group to receive rewards for invites. You can join by clicking: %s",
				hm.TrackedGroupInviteLink))
	}
	if !u.IsRegistered() {
		hm.Bot.Send(m.Sender, "You must have a wallet registered with your account to receive rewards.")
	}
	if hm.Disabled {
		hm.Bot.Send(m.Sender, DISABLED_MESSAGE_INVITES)
	}
	if notInGroup || !u.IsRegistered() {
		hm.Bot.Send(m.Sender, "To fully setup your account please use `/start` in a private message. Your link will still work but you will not receive rewards until you complete setup.")
	}
	hm.Bot.Send(m.Sender,
		fmt.Sprintf("Your referral link is: %s%d", hm.UserReferralLink, u.TelegramID))
}
