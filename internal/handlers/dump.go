package handlers

import (
	"fmt"
	"io/ioutil"
	"strings"
)

// Dump all waiting approvals in a format that can be executed
func (hm *HandlerManager) Dump() {
	hm.Usage.Report.CommandUsage.Dump++
	owed := make(map[string]uint64)
	pendingApprovals := hm.Users.ListApprovals()
	for _, userID := range pendingApprovals {
		userObj, ok := hm.Users.Load(userID)
		if ok {
			owed[userObj.Wallet] += hm.JoinReward
			if userObj.ReferrerID != 0 {
				referrerObj, ok := hm.Users.Load(userObj.ReferrerID)
				if ok {
					owed[referrerObj.Wallet] += hm.ReferralReward
				}
			}
		}
	}
	var owedString strings.Builder
	template := "spc wallet send scprimecoins %dSCP %s\n"
	for wallet, ballance := range owed {
		owedString.WriteString(fmt.Sprintf(template, ballance, wallet))
	}
	err := ioutil.WriteFile("owed_users_dump.txt", []byte(owedString.String()), 0777)
	if err != nil {
		hm.adminError("Failed to write to owed_users_dump.txt")
	}
}
