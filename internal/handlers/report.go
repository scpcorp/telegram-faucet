package handlers

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"

	tb "gopkg.in/tucnak/telebot.v2"
)

// Report provides a usage report
func (hm *HandlerManager) Report(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Report++
	hm.UpdateUsage()
	var report strings.Builder
	// Group reporting
	report.WriteString(fmt.Sprintf("Report for: %s", hm.TrackedGroup.Title))
	report.WriteString(fmt.Sprintf("\nGroup members: %d", hm.Usage.Report.GroupMembers))
	report.WriteString(fmt.Sprintf("\nTotal users: %d", hm.Usage.Report.UserStats.TotalUsers))
	report.WriteString(fmt.Sprintf("\nTotal join rewards: %d", hm.Usage.Report.UserStats.TotalJoinRewards))
	report.WriteString(fmt.Sprintf("\nTotal referrals: %d", hm.Usage.Report.UserStats.TotalReferrals))
	report.WriteString(fmt.Sprintf("\nCommand usage: %+v", hm.Usage.Report.CommandUsage))
	// get # messages in last week
	// get # messages from rewarded users
	// get # messages from non-rewarded users
	// get # of active rewarded users
	// get percentage of actively rewarded users
	// get total cost (rewards)
	// get average number of referrers
	// give top active folks
	// give top referrers
	hm.Bot.Send(m.Chat, report.String())
}

func logIf(err error) {
	if err != nil {
		logrus.Error(err)
	}
}

// UpdateUsage updates the hm.Usage elements that are not passively updated
func (hm *HandlerManager) UpdateUsage() {
	groupMembers, err := hm.Bot.Len(hm.TrackedGroup)
	logIf(err)
	hm.Usage.Report.GroupMembers = groupMembers
	hm.Usage.Report.UserStats = hm.Users.Stats()
}
