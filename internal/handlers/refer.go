package handlers

import (
	"fmt"
	"strconv"

	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
)

// Refer associates a referral code with a user
func (hm *HandlerManager) Refer(m *tb.Message) {
	hm.Usage.Report.CommandUsage.Refer++
	hm.referInternal(m)
}

func (hm *HandlerManager) referInternal(m *tb.Message) {
	code, err := strconv.Atoi(m.Payload)
	if err == nil {
		if code == m.Sender.ID {
			msg := fmt.Sprintf("User %s (%d) tried to refer themself, we did not set their referrer.", m.Sender.Username, m.Sender.ID)
			logrus.Info(msg)
			hm.Bot.Send(hm.AdminGroup, msg)
			hm.Bot.Send(m.Sender, "You can't refer yourself :(")
			return
		}
		if _, ok := hm.Users.GetReferringUser(code); ok {
			hm.Users.SetReferrer(m.Sender.ID, code)
			// TODO: Reference referring user's name here
			hm.Bot.Send(m.Sender, "We'll be sure to thank the user that referred you.")
			// Proceed with account creation check
			hm.StartCheckStatus(m)
			return
		}
	}
	hm.Bot.Send(m.Sender, "It looks like you have an invalid referral code. Usage is `/refer <code>`")
	logrus.Error(err)
}
