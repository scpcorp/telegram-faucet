package users

import (
	"sync"

	tb "gopkg.in/tucnak/telebot.v2"
)

// Users wraps all current users of the service and provide convenience methods
type Users struct {
	Path string
	Map  sync.Map
}

// User represents an individual user of the service
type User struct {
	TelegramID       int                `json:"telegram_id"`
	ReferrerID       int                `json:"referrer_id"`
	Wallet           string             `json:"wallet"`
	JoinReward       *Reward            `json:"join_reward"`
	ReferralRewards  map[string]*Reward `json:"referral_rewards"`
	PendingApprovals []*Approval        `json:"pending_approvals"`
}

// Approval holds the message and type of a reward penging approval
type Approval struct {
	Reason  string            `json:"reason"`
	Message *tb.StoredMessage `json:"message"`
}

// Reward holds the date and transaction for an issued reward
type Reward struct {
	TransactionIDs []string `json:"transaction_ids"`
	Date           string   `json:"date"`
}

// UserStats holds details on the users structure
type UserStats struct {
	TotalUsers       int
	TotalJoinRewards int
	TotalReferrals   int
}
