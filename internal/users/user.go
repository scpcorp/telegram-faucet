package users

import (
	tb "gopkg.in/tucnak/telebot.v2"
)

// IsRegistered checks if a user has associated a wallet with their telegram account
func (u *User) IsRegistered() bool {
	return u.Wallet != ""
}

// TBUser provides a telebot user struct
func (u *User) TBUser() *tb.User {
	return &tb.User{
		ID: u.TelegramID,
	}
}

// HasJoinReward checks if a user has been rewarding for joining the group
func (u *User) HasJoinReward() bool {
	return u.JoinReward != nil
}

// HasReferrer checks if a user was referred
func (u *User) HasReferrer() bool {
	return u.ReferrerID != 0
}

// HasPendingApproval checks to see if a user has a pending approval
func (u *User) HasPendingApproval() bool {
	return len(u.PendingApprovals) != 0
}

// CanMakeChanges determines if a user object is allowed to be changed
func (u *User) CanMakeChanges() bool {
	return len(u.PendingApprovals) == 0
}

// TotalReferrals gives a count of the number of refered users
func (u *User) TotalReferrals() int {
	return len(u.ReferralRewards)
}
