package users

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	tb "gopkg.in/tucnak/telebot.v2"
)

// Store stores a user in the user map
func (u *Users) Store(userID int, user *User) {
	u.Map.Store(userID, user)
}

// Load loads a user from the user map
func (u *Users) Load(userID int) (*User, bool) {
	v, ok := u.Map.Load(userID)
	if !ok {
		return nil, ok
	}
	return v.(*User), ok
}

// LoadOrCreate creates and stores a new user
func (u *Users) LoadOrCreate(userID int) (*User, bool) {
	v, ok := u.Map.LoadOrStore(userID, &User{
		TelegramID: userID, // TODO: Fill out the struct more?
	})
	return v.(*User), ok
}

// CreateWithReferrer creates and stores a new user with a referral code
func (u *Users) CreateWithReferrer(userID int, referrerID int) (*User, bool) {
	v, ok := u.Map.LoadOrStore(userID, &User{
		TelegramID: userID,
		ReferrerID: referrerID,
	})
	return v.(*User), !ok
}

// GetReferringUser retrieves a user with a matching referral code if one exists
func (u *Users) GetReferringUser(userID int) (*User, bool) {
	return u.Load(userID)
}

// SetReferrer sets the referrer on a specific user
func (u *Users) SetReferrer(userID int, code int) bool {
	if user, ok := u.LoadOrCreate(userID); ok && user.CanMakeChanges() {
		user.ReferrerID = code
		u.Store(userID, user)
		return true
	}
	return false
}

// SetWallet sets the wallet on a specific user
func (u *Users) SetWallet(userID int, wallet string) bool {
	if user, ok := u.LoadOrCreate(userID); ok && user.CanMakeChanges() {
		user.Wallet = wallet
		u.Store(userID, user)
		return true
	}
	return false
}

// IsWalletInUse sets the wallet on a specific user
func (u *Users) IsWalletInUse(wallet string) bool {
	inUse := false
	u.Map.Range(func(k, v interface{}) bool {
		userObj := v.(*User)
		if userObj.Wallet == wallet {
			return false
		}
		return true
	})
	return inUse
}

func toReward(transactions []string) *Reward {
	return &Reward{
		TransactionIDs: transactions,
		Date:           time.Now().Format(time.RFC3339),
	}
}

// SetJoinReward sets the join reward on a specific user
func (u *Users) SetJoinReward(userID int, transactions []string) bool {
	if user, ok := u.LoadOrCreate(userID); ok {
		if user.HasJoinReward() {
			return false
		}
		user.JoinReward = toReward(transactions)
		u.Store(userID, user)
		_, err := u.SaveUsers()
		if err != nil {
			logrus.Error(err)
		}
		return true
	}
	return false
}

// AddReferralReward add referral reward tracks a successful referral
func (u *Users) AddReferralReward(userID int, referredUserID int, transactions []string) bool {
	if user, ok := u.Load(userID); ok {
		if user.ReferralRewards == nil {
			user.ReferralRewards = make(map[string]*Reward)
		}
		strID := strconv.Itoa(referredUserID)
		if _, ok := user.ReferralRewards[strID]; ok {
			return false
		}
		user.ReferralRewards[strID] = toReward(transactions)
		u.Store(userID, user)
		_, err := u.SaveUsers()
		if err != nil {
			logrus.Error(err)
		}
		return true
	}
	return false
}

// AddApproval adds a tracked approval message to a user
func (u *Users) AddApproval(userID int, reason string, message *tb.StoredMessage) bool {
	if user, ok := u.Load(userID); ok {
		user.PendingApprovals = append(user.PendingApprovals, &Approval{
			Reason:  reason,
			Message: message,
		})
		u.Store(userID, user)
		_, err := u.SaveUsers()
		if err != nil {
			logrus.Error(err)
		}
		return true
	}
	return false
}

// ListApprovals returns a list of users waiting to be approved
func (u *Users) ListApprovals() []int {
	var approvals []int
	u.Map.Range(func(key interface{}, value interface{}) bool {
		u := value.(*User)
		if len(u.PendingApprovals) > 0 {
			approvals = append(approvals, u.TelegramID)
		}
		return true
	})
	return approvals
}

// PopApproval removes and returns the first approval msg for a user
func (u *Users) PopApproval(userID int) *Approval {
	if user, ok := u.Load(userID); ok {
		if len(user.PendingApprovals) < 1 {
			return nil
		}
		var approval *Approval
		approval, user.PendingApprovals = user.PendingApprovals[0], user.PendingApprovals[1:]
		u.Store(userID, user)
		_, err := u.SaveUsers()
		if err != nil {
			logrus.Error(err)
		}
		return approval
	}
	return nil
}

// IdentifyUsersMissingReferral finds users who didn't have their referral rewarded
func (u *Users) IdentifyUsersMissingReferral() []*User {
	var usersMissingReferral []*User
	u.Map.Range(func(k, v interface{}) bool {
		userID := k.(int)
		userObj := v.(*User)
		strID := strconv.Itoa(userID)
		if userObj.HasReferrer() && userObj.HasJoinReward() {
			if referrerObj, ok := u.Load(userObj.ReferrerID); ok {
				if referrerObj.ReferralRewards == nil {
					usersMissingReferral = append(usersMissingReferral, userObj)
				} else if _, ok := referrerObj.ReferralRewards[strID]; !ok {
					usersMissingReferral = append(usersMissingReferral, userObj)
				}
			}
		}
		return true
	})
	return usersMissingReferral
}

// LoadUsers loads the sync.Map from disk, as sync.Map itself
// isn't able to be unmarshalled into
func (u *Users) LoadUsers() (int, error) {
	var tmpMap map[int]User
	var loadedUsers int
	data, err := ioutil.ReadFile(u.Path)
	if err != nil {
		logrus.Errorf("Error: %v", err)
		return 0, err
	}
	if err := json.Unmarshal(data, &tmpMap); err != nil {
		logrus.Error(err)
		return 0, err
	}
	for key := range tmpMap {
		user := tmpMap[key]
		u.Store(key, &user)
		loadedUsers++
	}
	return loadedUsers, nil
}

// SaveUsers saves the sync.Map to disk, as sync.Map itself
// isn't able to be marshalled
func (u *Users) SaveUsers() (int, error) {
	tmpMap := make(map[int]*User)
	var savedUsers int
	u.Map.Range(func(k, v interface{}) bool {
		userID := k.(int)
		userObj := v.(*User)
		tmpMap[userID] = userObj
		savedUsers++
		return true
	})
	mapBytes, err := json.Marshal(tmpMap)
	if err != nil {
		logrus.Error(err)
		return 0, err
		// TODO should probably catch this, as otherwise everything is lost
	}
	err = ioutil.WriteFile(u.Path, mapBytes, 0777)
	if err != nil {
		logrus.Error(err)
		return 0, err
		// TODO should probably catch this, as otherwise everything is lost
	}
	return savedUsers, nil
}

// StartUsersBackup starts a backup thread for periodically saving the users to disk
func (u *Users) StartUsersBackup(shutdown <-chan int, done chan<- bool) {
	for {
		select {
		case <-shutdown:
			logrus.Info("Stopping user backup thread.")
			u.backupUsers()
			done <- true
			return
		case <-time.After(15 * time.Minute):
			u.backupUsers()
		}
	}
}

// Stats returns statistics on the userbase
func (u *Users) Stats() *UserStats {
	stats := &UserStats{}
	u.Map.Range(func(key interface{}, value interface{}) bool {
		u := value.(*User)
		stats.TotalReferrals += u.TotalReferrals()
		if u.HasJoinReward() {
			stats.TotalJoinRewards++
		}
		stats.TotalUsers++
		return true
	})
	return stats
}

func (u *Users) backupUsers() {
	logrus.Debug("Copying old users file.")
	original, err := os.Open(u.Path)
	backup, err := os.OpenFile(u.Path+".bak", os.O_RDWR|os.O_CREATE, 0666)
	_, err = io.Copy(backup, original)
	if err != nil {
		logrus.Error(err)
	}
	logrus.Debug("Storing users...")
	saved, err := u.SaveUsers()
	if err != nil {
		logrus.Fatal("Failed to save users!")
	}
	logrus.Infof("Saved %d users.", saved)
}
