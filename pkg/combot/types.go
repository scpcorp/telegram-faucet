package combot

// CheckResponse is the response to a combot check request
type CheckResponse struct {
	Ok          bool   `json:"ok"`
	Description string `json:"description"`
}
