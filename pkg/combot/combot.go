package combot

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

var baseURL = "https://api.cas.chat/"

// Check checks if a user is Combot blocked
func Check(id int) (*CheckResponse, error) {
	rawResponse, err := request(fmt.Sprintf("check?user_id=%d", id))
	if err != nil {
		return nil, err
	}
	var response CheckResponse
	if err := json.Unmarshal(rawResponse, &response); err != nil {
		return nil, err
	}
	return &response, nil
}

func request(args ...string) ([]byte, error) {
	reqResponse, _ := http.Get(baseURL + strings.Join(args, "/"))
	return ioutil.ReadAll(reqResponse.Body)
}
