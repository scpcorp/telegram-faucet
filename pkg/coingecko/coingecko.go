package coingecko

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/SiaPrime/telegram-faucet/internal/util"
)

var baseURL = "https://api.coingecko.com/api/v3/"

func request(args ...string) ([]byte, error) {
	reqResponse, _ := http.Get(baseURL + strings.Join(args, "/"))
	return ioutil.ReadAll(reqResponse.Body)
}

// New creates a Client with the ability to validate coins
func New() (*Client, error) {
	var client Client
	client.coins = make(map[string]Coin)
	var err error = nil
	err = client.PopulateCoins()
	return &client, err
}

// NewSingleCoin creates a Client with the ability to validate one coin
func NewSingleCoin(coin Coin) *Client {
	var client Client
	client.coins = make(map[string]Coin)
	client.singleCoin = coin.Name
	client.coins[strings.ToUpper(coin.Name)] = coin
	return &client
}

// PopulateCoins gets a list of coins you can query at coingecko
func (client *Client) PopulateCoins() error {
	rawResponse, err := request("coins/list")
	if err != nil {
		return err
	}
	var response coinsListResponse
	if err := json.Unmarshal(rawResponse, &response); err != nil {
		return err
	}
	for _, coin := range response {
		client.coins[strings.ToUpper(coin.Symbol)] = Coin{
			ID:   coin.ID,
			Name: coin.Name,
		}
	}
	return nil
}

func (client *Client) getCoin(coin string) (Coin, bool) {
	coinObj, ok := client.coins[strings.ToUpper(coin)]
	return coinObj, ok
}

// Status returns the status for the single coin of a single coin client, errors if one doesn't exist
func (client *Client) Status() (CoinStatus, error) {
	println(client.singleCoin)
	return client.GetCoinStatus(client.singleCoin)
}

// GetCoinStatus returns a coin ID and true if a coin is valid, or false if it is not
func (client *Client) GetCoinStatus(coin string) (CoinStatus, error) {
	if coinObj, ok := client.getCoin(coin); ok {
		rawResponse, err := request("coins/", coinObj.ID)
		if err != nil {
			return CoinStatus{}, err
		}
		var response coinResponse
		if err := json.Unmarshal(rawResponse, &response); err != nil {
			return CoinStatus{}, err
		}
		return CoinStatus{
			Name:           coinObj.Name,
			PriceUSD:       "$" + util.F(response.MarketData.CurrentPrice.Usd, false),
			PriceBTC:       "₿" + util.F(response.MarketData.CurrentPrice.Btc, false),
			MarketCapUSD:   "$" + util.F(response.MarketData.MarketCap.Usd, true),
			High24H:        "$" + util.F(response.MarketData.High24H.Usd, true),
			Low24H:         "$" + util.F(response.MarketData.Low24H.Usd, true),
			Supply:         util.F(response.MarketData.CirculatingSupply, false),
			PriceChange24H: fmt.Sprintf("%.02f%%", response.MarketData.PriceChangePercentage24H),
			PriceChange7D:  fmt.Sprintf("%.02f%%", response.MarketData.PriceChangePercentage7D),
			PriceChange30D: fmt.Sprintf("%.02f%%", response.MarketData.PriceChangePercentage30D),
		}, nil
	}
	return CoinStatus{}, errors.New("Invalid coin passed")
}
