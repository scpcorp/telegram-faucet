package sia

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

// New creates a new siad client
func New(address, password, agent string) *Client {
	client := &Client{
		Address:   address,
		Password:  password,
		UserAgent: agent,
		HTTPClient: &http.Client{
			Timeout: time.Second * 30,
		},
	}
	constants, err := client.Constants()
	if err != nil {
		logrus.Fatal(err)
	}
	if constants.ScPrimeCoinPrecision != "" {
		client.precision, _ = new(big.Int).SetString(constants.ScPrimeCoinPrecision, 10)
		logrus.Info("Using ScPrimeCoinPrecision from /daemon/constants.")
	} else {
		client.precision, _ = new(big.Int).SetString(constants.SiacoinPrecision, 10)
		logrus.Info("Using SiacoinPrecision from /daemon/constants.")
	}
	return client
}

// Consensus gets consensus details from a siad instance
func (client *Client) Consensus() (ConsensusResponse, error) {
	raw, err := client.request("GET", "", "consensus")
	if err != nil {
		return ConsensusResponse{}, err
	}

	var response ConsensusResponse
	err = json.Unmarshal(raw, &response)
	return response, err
}

// Send sends coins somewhere
func (client *Client) Send(coins uint64, destination string) (SiacoinsResponse, error) {
	coinsHastings := client.CoinsToHastings(coins)
	balance, err := client.Balance()
	if err != nil {
		return SiacoinsResponse{}, err
	}
	cmpResult := balance.Cmp(coinsHastings)
	if cmpResult == -1 {
		msg := fmt.Sprintf("Insufficient balance in wallet:\nBalH: %s\nAskH: %s\nBalC: %s\nAskC: %s\nResult: %d",
			balance.String(), coinsHastings.String(),
			client.HastingsToCoins(balance).String(), string(coins),
			cmpResult)
		return SiacoinsResponse{}, errors.New(msg)
	}
	body := fmt.Sprintf("amount=%s&destination=%s", coinsHastings, destination)
	raw, err := client.request("POST", body, "wallet", "siacoins")
	if err != nil {
		return SiacoinsResponse{}, err
	}
	var response SiacoinsResponse
	err = json.Unmarshal(raw, &response)
	return response, err
}

// IsValidAddr validates a provided address
func (client *Client) IsValidAddr(addr string) (bool, error) {
	raw, err := client.request("GET", "", "wallet", "verify", "address", addr)
	if err != nil {
		return false, err
	}
	var response ValidAddressResponse
	err = json.Unmarshal(raw, &response)
	return response.Valid, err
}

// Balance gets the active wallet balance
func (client *Client) Balance() (balHastings *big.Int, err error) {
	raw, err := client.request("GET", "", "wallet")
	if err != nil {
		return &big.Int{}, err
	}
	var response WalletResponse
	err = json.Unmarshal(raw, &response)
	if err != nil {
		return &big.Int{}, err
	}
	confirmed, _ := new(big.Int).SetString(response.ConfirmedSiacoinBalance, 10)
	unconfirmedLeaving, _ := new(big.Int).SetString(response.UnconfirmedOutgoingSiacoins, 10)
	unconfirmedIncoming, _ := new(big.Int).SetString(response.UnconfirmedIncomingSiacoins, 10)
	unconfirmed := new(big.Int).Sub(unconfirmedIncoming, unconfirmedLeaving)
	minimumBalance := new(big.Int).Add(confirmed, unconfirmed)
	return minimumBalance, nil
}

// Constants return the constants used by the daemon
func (client *Client) Constants() (*ConstantsResponse, error) {
	raw, err := client.request("GET", "", "daemon", "constants")
	if err != nil {
		return nil, err
	}
	var response ConstantsResponse
	err = json.Unmarshal(raw, &response)
	return &response, err
}

// CoinsToHastings converts a coins to an api compatible number
func (client *Client) CoinsToHastings(coins uint64) *big.Int {
	// 10^24 (hastings unit)
	coinsBig := new(big.Int).SetUint64(coins)
	return coinsBig.Mul(coinsBig, client.precision)
}

// HastingsToCoins gets the number of coins from hastings
func (client *Client) HastingsToCoins(hastings *big.Int) *big.Int {
	return new(big.Int).Div(hastings, client.precision)
}

func (client *Client) request(method string, body string, args ...string) ([]byte, error) {
	url := client.requestURL(args)
	logrus.Debugf("Request (%s):\n\tBody: %s\n\tURL: %s", method, body, url)
	req, err := http.NewRequest(method, url, strings.NewReader(body))
	if err != nil {
		println(err.Error())
		return nil, err
	}
	req.SetBasicAuth("", client.Password)
	req.Header.Add("User-Agent", client.UserAgent)
	if method == "POST" {
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	}
	reqResponse, err := client.HTTPClient.Do(req)
	if err != nil {
		println(err.Error())
		return nil, err
	}
	v, err := ioutil.ReadAll(reqResponse.Body)
	return v, err
}

func (client *Client) requestURL(args []string) string {
	return client.Address + strings.Join(args, "/")
}
