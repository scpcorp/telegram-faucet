package sia

import (
	"math/big"
	"net/http"
)

// Client is an API client for siad
type Client struct {
	Address    string
	Password   string
	UserAgent  string
	HTTPClient *http.Client
	precision  *big.Int
}

// ConsensusResponse is the response from the daemon's consensus endpoint
type ConsensusResponse struct {
	Synced                 bool   `json:"synced"`
	Height                 int    `json:"height"`
	Currentblock           string `json:"currentblock"`
	Target                 []int  `json:"target"`
	Difficulty             string `json:"difficulty"`
	Blockfrequency         int    `json:"blockfrequency"`
	Blocksizelimit         int    `json:"blocksizelimit"`
	Extremefuturethreshold int    `json:"extremefuturethreshold"`
	Futurethreshold        int    `json:"futurethreshold"`
	Genesistimestamp       int    `json:"genesistimestamp"`
	Maturitydelay          int    `json:"maturitydelay"`
	Mediantimestampwindow  int    `json:"mediantimestampwindow"`
	SiafundCount           string `json:"siafundcount"`
	SiafundPortion         string `json:"siafundportion"`
	Initialcoinbase        int    `json:"initialcoinbase"`
	Minimumcoinbase        int    `json:"minimumcoinbase"`
	Roottarget             []int  `json:"roottarget"`
	Rootdepth              []int  `json:"rootdepth"`
	SiacoinPrecision       string `json:"siacoinprecision"`
}

// SiacoinsResponse is the response from the daemon's /wallet/siacoins endpoint
type SiacoinsResponse struct {
	Transactionids []string `json:"transactionids"`
}

// ValidAddressResponse is the response from the daemon's /wallet/verify/address endpoint
type ValidAddressResponse struct {
	Valid bool `json:"valid"`
}

// WalletResponse is the response from the daemon's /wallet endpoint
type WalletResponse struct {
	Encrypted                   bool   `json:"encrypted"`
	Unlocked                    bool   `json:"unlocked"`
	Rescanning                  bool   `json:"rescanning"`
	ConfirmedSiacoinBalance     string `json:"confirmedsiacoinbalance"`
	UnconfirmedOutgoingSiacoins string `json:"unconfirmedoutgoingsiacoins"`
	UnconfirmedIncomingSiacoins string `json:"unconfirmedincomingsiacoins"`
	SiafundBalance              string `json:"siafundbalance"`
	SiacoinClaimBalance         string `json:"siacoinclaimbalance"`
	DustThreshold               string `json:"dustthreshold"`
}

// ConstantsResponse is the response from the daemon's /daemon/constants endpoint
type ConstantsResponse struct {
	BlockFrequency         int    `json:"blockfrequency"`
	BlocksizeLimit         int    `json:"blocksizelimit"`
	ExtremeFutureThreshold int    `json:"extremefuturethreshold"`
	FutureThreshold        int    `json:"futurethreshold"`
	GenesisTimestamp       int    `json:"genesistimestamp"`
	MaturityDelay          int    `json:"maturitydelay"`
	MedianTimestampWindow  int    `json:"mediantimestampwindow"`
	SiafundCount           string `json:"siafundcount"`
	SiafundPortion         string `json:"siafundportion"`
	TargetWindow           int    `json:"targetwindow"`
	InitialCoinbase        int    `json:"initialcoinbase"`
	MinimumCoinbase        int    `json:"minimumcoinbase"`
	RootTarget             []int  `json:"roottarget"`
	RootDepth              []int  `json:"rootdepth"`
	DefaultAllowance       struct {
		Funds              string `json:"funds"`
		Hosts              int    `json:"hosts"`
		Period             int    `json:"period"`
		RenewWindow        int    `json:"renewwindow"`
		ExpectedStorage    int64  `json:"expectedstorage"`
		ExpectedUpload     int    `json:"expectedupload"`
		ExpectedDownload   int    `json:"expecteddownload"`
		ExpectedRedundancy int    `json:"expectedredundancy"`
	} `json:"defaultallowance"`
	MaxAdjustmentUp         string `json:"maxadjustmentup"`
	MaxAdjustmentDown       string `json:"maxadjustmentdown"`
	MaxTargetAdjustmentUp   string `json:"maxtargetadjustmentup"`
	MaxTargetAdjustmentDown string `json:"maxtargetadjustmentdown"`
	SiacoinPrecision        string `json:"siacoinprecision"`
	ScPrimeCoinPrecision    string `json:"scprimecoinprecision"`
}
