# Telegram Faucet
This bot exists to provide rewards to users for engaging with the ScPrime community on telegram.

## Initial Setup
// TODO: Finish up readme setup instructions
1. Copy the example configuration file `primebot.example.json` to `primebot.json` in the directory you will be launching the bot from.
2. Create a telegram bot with @BotFather on telegram.
3. set commands
    ```
    stats - Get the current status of the ScPrime network.
    invite - Get an invite link to share with other users.
    register - Register a wallet address to your account. Usage is: /register <wallet>
    refer - Mark a user as your referrer. Usage is: /refer <referrerID>
    ```
4. configure api key and link
5. setup spd daemon
6. After launching the daemon, the `"sia": { "password":"<value>"}` in `primebot.json` can be found using instructions in the [daemon API documentation](https://gitlab.com/NebulousLabs/Sia/blob/master/doc/api/index.html.md#authentication).
7. add monitored group and admins
8. add to group / config

## Building
1. Navigate to `<project root>/cmd/primebot`.
2. Run `go build -ldflags "-s -w"`
3. Copy/move `primebot` to wherever you would like to execute it.

## Running
1. Start your ScPrime/Sia daemon.
2. Ensure you have a `primebot.json` in your working directory.
3. Run the bot executable or `go run cmd/primebot/main.go`.

## Author
This project was created by [@not-inept](https://notinept.me)